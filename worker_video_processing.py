# -*- coding: utf-8 -*-
import os
import time
import traceback
import logging as log
import re
import tempfile
# noinspection PyPackageRequirements
import magic

from PyQt5 import QtCore
from queue import Queue
import subprocess as sub

__author__ = 'ozgur'
__creation_date__ = '28.08.2016' '20:58'

TMP_DIR_SUFFIX = "temp"
OUT_DIR_SUFFIX = "output"
DELET_DIR_SUFFIX = "deleted"
VC_MESSAGE_QUEUE = Queue()


class ConversionPresets:
    ultrafast = "ultrafast"
    superfast = "superfast"
    veryfast = "veryfast"
    faster = "faster"
    fast = "fast"
    medium = "medium"
    slow = "slow"
    slower = "slower"
    veryslow = "veryslow"
    placebo = "placebo"


# noinspection PyTypeChecker,PyBroadException
class VideoFile:
    def __init__(self, name, basedir, uid):
        self.uid = uid
        self.name = name
        self.basedir = basedir
        self.filepath = os.path.join(basedir, name)
        self.definition = name
        self.duration = None
        self.bitrate = None
        self.video_codec = None
        self.video_profile = None
        self.video_resolution = None
        self.video_resolution_w = None
        self.video_resolution_h = None
        self.video_bitrate = None
        self.video_fps = None
        self.video_framecount = None
        self.audio_codec = None
        self.audio_frequency = None
        self.audio_bitrate = None

    def fill_video_details(self):
        """
        Delivered From https://gist.github.com/jaivikram/4690569
        :return: None
        """
        try:
            tmpf = tempfile.NamedTemporaryFile()
            os.system("ffmpeg -i \"%s\" 2> %s" % (self.filepath, tmpf.name))
            lines = tmpf.readlines()
            tmpf.close()
            # metadata = {}
            for l in lines:
                l = l.decode("utf-8")
                l = l.strip()
                if l.startswith('Duration'):
                    self.duration = re.search('Duration: (.*?),', l).group(0).split(':', 1)[1].strip(' ,')
                    self.bitrate = re.search('bitrate: (\d+ kb/s)', l).group(0).split(':')[1].strip()
                if l.startswith('Stream #0:0'):
                    self.video_codec, self.video_profile = [e.strip(' ,()') for e in re.search('Video: (.*? \(.*?\)),? ', l).group(0).split(':')[1].split('(')]
                    self.video_resolution = re.search('([1-9]\d+x\d+)', l).group(1)
                    if len(self.video_resolution.split("x")) == 2:
                        self.video_resolution_w, self.video_resolution_h = self.video_resolution.split("x")
                    self.video_bitrate = re.search('(\d+ kb/s)', l).group(1)
                    # self.video_fps = (re.search('(\d+ fps)', l).group(1)).split(" ")[0]
                    try:
                        self.video_fps = (re.search('(\d+.\d+ fps)', l).group(1)).split(" ")[0]
                    except Exception:
                        self.video_fps = (re.search('(\d+ fps)', l).group(1)).split(" ")[0]
                if l.startswith('Stream #0:1'):
                    self.audio_codec = re.search('Audio: (.*?) ', l).group(1)
                    self.audio_frequency = re.search(', (.*? Hz),', l).group(1)
                    self.audio_bitrate = re.search(', (\d+ kb/s)', l).group(1)

            h, m, s = self.duration.split(".")[0].split(":")
            self.video_framecount = int((int(s) + (int(m) * 60) + (int(h) * 3600)) * float(self.video_fps))
            self.definition = "T:{0} R:{1} F:{2} N:{3}".format(self.duration.split(".")[0], (self.video_resolution + "         ")[:9], (str(self.video_framecount) + "            ")[:12], self.name)
        except Exception:
            log.error(traceback.format_exc())
        return None


class VideoSet:
    def __init__(self, videofile, preset, crf, container_type, resolution, thread_count=1):
        self.videofile = videofile
        self.preset = preset
        self.crf = crf
        self.thread_count = thread_count
        self.container_type = container_type
        self.resolution = resolution


# noinspection PyArgumentList
class QtScanFolderForVideoFiles(QtCore.QThread):
    signal_scan_finished = QtCore.pyqtSignal(object)

    def __init__(self, path, nextuid):
        QtCore.QThread.__init__(self)
        self.path = path
        self.nextuid = nextuid

    def run(self):
        ret_list = []
        if self.path:
            for file_name in os.listdir(self.path):
                try:
                    file_path = os.path.join(self.path, file_name)
                    mime = magic.Magic(mime=True)
                    if not os.path.isdir(file_path) and mime.from_file(file_path).split("/")[0] == "video":
                        videofile = VideoFile(file_name, self.path, self.nextuid)
                        self.nextuid += 1

                        videofile.fill_video_details()
                        ret_list.append(videofile)
                except Exception:
                    log.error(traceback.format_exc())

        self.signal_scan_finished.emit(ret_list)
        # return ret_list


# noinspection PyArgumentList,PyBroadException
class QtConvertVideo(QtCore.QThread):
    signal_got_message = QtCore.pyqtSignal(object)
    signal_single_video_converting = QtCore.pyqtSignal(object)
    signal_single_video_converted = QtCore.pyqtSignal(object)
    signal_queue_finished = QtCore.pyqtSignal()

    def __init__(self, videoset_list):
        QtCore.QThread.__init__(self)
        self.videoset_list = videoset_list

    def run(self):
        for videoset in self.videoset_list:
            try:
                self.signal_single_video_converting.emit(videoset.videofile)
                if videoset.crf > 51:
                    videoset.crf = 51  # Worst
                if videoset.crf < 0:
                    videoset.crf = 0  # loseless

                tmp_dir = os.path.join(videoset.videofile.basedir, TMP_DIR_SUFFIX)
                out_dir = os.path.join(videoset.videofile.basedir, OUT_DIR_SUFFIX)
                in_file = os.path.join(videoset.videofile.basedir, videoset.videofile.name)
                out_file = os.path.join(videoset.videofile.basedir, TMP_DIR_SUFFIX, os.path.splitext(videoset.videofile.name)[0] + "." + videoset.container_type)
                save_file = os.path.join(videoset.videofile.basedir, OUT_DIR_SUFFIX, os.path.splitext(videoset.videofile.name)[0] + "." + videoset.container_type)

                if not os.path.exists(tmp_dir):
                    os.mkdir(tmp_dir)
                if os.path.exists(out_file):
                    os.remove(out_file)

                vf = "-vf"
                if videoset.videofile.video_resolution_h is None or videoset.videofile.video_resolution_w is None:
                    scale = "scale=-1:" + videoset.resolution
                elif videoset.resolution == "None":
                    scale = None
                    vf = None
                elif int(videoset.videofile.video_resolution_w) > int(videoset.videofile.video_resolution_h) > int(videoset.resolution):
                    scale = "scale=-1:" + videoset.resolution
                elif int(videoset.videofile.video_resolution_h) > int(videoset.videofile.video_resolution_w) > int(videoset.resolution):
                    scale = "scale=" + videoset.resolution + ":-1"
                else:
                    scale = None
                    vf = None

                command = [
                    "ffmpeg",
                    "-i",
                    in_file,
                    vf,
                    scale,
                    "-c:v",
                    "libx264",
                    "-preset",
                    str(videoset.preset),
                    "-crf",
                    str(videoset.crf),
                    "-c:a",
                    "copy",
                    "-threads",
                    str(videoset.thread_count),
                    out_file
                ]
                command = [x for x in command if x is not None]
                log.info(" ".join(command))

                process = sub.Popen(command, stdout=sub.PIPE, stderr=sub.STDOUT, universal_newlines=True)
                for line in process.stdout:
                    self.signal_got_message.emit(line)

                time.sleep(2)
                if not os.path.exists(out_dir):
                    os.mkdir(out_dir)

                if os.path.exists(save_file):
                    os.remove(save_file)
                os.rename(out_file, save_file)

                self.signal_single_video_converted.emit(videoset.videofile)
            except FileNotFoundError:
                log.warning("Converting failed for file ->" + videoset.videofile.filepath)
            except Exception:
                log.error(traceback.format_exc())

        self.signal_queue_finished.emit()
