# -*- coding: utf-8 -*-
from PyQt5.QtGui import QColor

__author__ = 'ozgur'
__creation_date__ = '17.09.2016' '23:16'

LOG_SPLITTER = ">>"
LOG_FILE_PATH = 'imagetoolkit.log'
LOGFORMAT = "%(asctime)s " + LOG_SPLITTER + " %(levelname)s " + LOG_SPLITTER + " %(message)s"
FRAME_KEYWORD = "frame"

RED = QColor(155, 0, 0)
YELLOW = QColor(155, 155, 0)
GREEN = QColor(0, 155, 0)
BLUE = QColor(0, 155, 0)
