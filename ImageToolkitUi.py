import logging as log
import os
import re
import subprocess as sub
import traceback

from PyQt5 import QtCore
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QFileDialog, QMainWindow, QListWidgetItem

from ImageToolkitUi_design import Ui_MainWindow
from config import LOG_FILE_PATH, LOG_SPLITTER, RED, GREEN, YELLOW, BLUE, FRAME_KEYWORD
from worker_video_processing import VideoSet, OUT_DIR_SUFFIX, QtConvertVideo, QtScanFolderForVideoFiles


# noinspection PyPep8Naming,PyBroadException
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)

        # nongui
        self.vt_nextuid = 0
        self.vt_scanner = QtScanFolderForVideoFiles("", self.vt_nextuid)
        self.vt_videoconverter = QtConvertVideo([])

        self.vt_total_frame = 0
        self.vt_transformed_frame = 0
        self.vt_sub_total_frame = 0
        self.vt_sub_transformed_frame = 0
        self.vt_ffmpeg_has_error = False

        # Signals/slots - Video Transform
        self.vt_btnSelectFolder.clicked.connect(self.event_vt_btnSelectFolder_clicked)
        self.vt_btnPlay.clicked.connect(self.event_vt_btnPlay_clicked)
        self.vt_btnRemove.clicked.connect(self.event_vt_btnRemove_clicked)
        self.vt_btnConvert.clicked.connect(self.event_vt_btnConvert_clicked)
        self.vt_btnConvertStop.clicked.connect(self.event_vt_btnConvertStop_clicked)
        self.vt_btnDelete.clicked.connect(self.event_vt_btnDelete_clicked)
        self.vt_btnRequeue.clicked.connect(self.event_vt_btnRequeue_clicked)

        # Signals/slots - Main
        self.tabMain.currentChanged.connect(self.event_lg_tabMain_currentchanged)

        # Signals/slots - Multitool
        self.mt_btnSelectFolder.clicked.connect(self.event_mt_btnSelectFolder_clicked)
        self.mt_btnOrderByNumber.clicked.connect(self.event_mt_btnOrderByNumber_clicked)

    # GLOBALS
    # noinspection PyMethodMayBeStatic
    def percent(self, part, whole):
        try:
            retval = int(100 * float(part) / float(whole))
        except ZeroDivisionError as a:
            retval = 0
            log.warning("main.py > percent > ZeroDivisionError")
        return retval

    def event_lg_tabMain_currentchanged(self, selectedIndex):
        tabMainIndex = 2
        if selectedIndex == tabMainIndex:
            self.lg_fill_log_list()

    # VIDEO TRANSFORM

    def event_vt_btnSelectFolder_clicked(self):
        try:
            # noinspection PyCallByClass,PyTypeChecker,PyArgumentList
            directory_path = QFileDialog.getExistingDirectory(self, "Pick Videos Folder")
            self.vt_txtPath.setText(directory_path)
            self.vt_scanner.terminate()
            self.vt_scanner = QtScanFolderForVideoFiles(directory_path, self.vt_nextuid)
            self.vt_scanner.signal_scan_finished.connect(self.slot_vt_scan_folder_finished)
            self.vt_scanner.start()
            self.vt_change_enable_status_all(False)
            self.statusbar.showMessage("Scanning " + directory_path + "...")
        except Exception:
            log.error(traceback.format_exc())

    def event_vt_btnPlay_clicked(self):
        for item in self.vt_lstWaiting.selectedItems():
            try:
                vf = item.data(QtCore.Qt.UserRole)
                vfpath = os.path.join(vf.basedir, vf.name)
                # sub.Popen(["mplayer", vfpath], stdout=sub.PIPE)
                sub.Popen(["ffplay", vfpath], stdout=sub.PIPE)
            except Exception:
                log.error(traceback.format_exc())

    def event_vt_btnRemove_clicked(self):
        for item in self.vt_lstWaiting.selectedItems():
            try:
                self.vt_lstWaiting.takeItem(self.vt_lstWaiting.row(item))
            except Exception:
                log.error(traceback.format_exc())

    def event_vt_btnConvert_clicked(self):
        try:
            # prepare
            self.vt_reset_progress_bars()
            self.vt_change_enable_status_all(False)
            self.vt_btnConvertStop.setEnabled(True)
            lstWaiting_items = []
            videoset_list = []

            # collect convert parameters
            preset = self.vt_cmbPreset.currentText()
            crf = self.vt_txtnumCrf.value()
            thread_count = self.vt_txtnumThread.value()
            container_type = self.vt_cmbContainerType.currentText()
            resolution = self.vt_cmbResolution.currentText()

            # collect video files to convert
            for index in range(self.vt_lstWaiting.count()):
                lstWaiting_items.append(self.vt_lstWaiting.item(index))
            for item in lstWaiting_items:
                videofile = item.data(QtCore.Qt.UserRole)
                self.vt_total_frame += int(videofile.video_framecount)
                videoset_list.append(VideoSet(videofile, preset, crf, container_type, resolution, thread_count))

            try:
                self.vt_videoconverter.terminate()
            except:
                pass
            self.vt_videoconverter = QtConvertVideo(videoset_list)
            self.vt_videoconverter.signal_got_message.connect(self.slot_vt_got_message)
            self.vt_videoconverter.signal_single_video_converting.connect(self.slot_vt_single_video_converting)
            self.vt_videoconverter.signal_single_video_converted.connect(self.slot_vt_single_video_converted)
            self.vt_videoconverter.signal_queue_finished.connect(self.slot_vt_queue_finished)
            self.vt_videoconverter.start()
        except Exception:
            log.error(traceback.format_exc())

    def event_vt_btnConvertStop_clicked(self):
        try:
            self.vt_videoconverter.terminate()
            self.vt_lstProcessing.clear()
        except:
            pass
        self.vt_change_enable_status_all(True)

    def event_vt_btnDelete_clicked(self):
        for item in self.vt_lstWaiting.selectedItems():
            try:
                videofile = item.data(QtCore.Qt.UserRole)
                delete_dir = os.path.join(videofile.basedir, OUT_DIR_SUFFIX)
                in_file = os.path.join(videofile.basedir, videofile.name)
                out_file = os.path.join(videofile.basedir, OUT_DIR_SUFFIX, videofile.name)
                if not os.path.exists(delete_dir):
                    os.mkdir(delete_dir)
                os.rename(in_file, out_file)
                self.vt_lstWaiting.takeItem(self.vt_lstWaiting.row(item))
            except Exception:
                log.error(traceback.format_exc())

    def event_vt_btnRequeue_clicked(self):
        for item in self.vt_lstDone.selectedItems():
            try:
                videofile=item.data(QtCore.Qt.UserRole)
                item = QListWidgetItem(videofile.definition)
                item.setData(QtCore.Qt.UserRole, videofile)
                self.vt_lstWaiting.addItem(item)
                # self.vt_lstDone.takeItem(self.vt_lstDone.row(item))
            except Exception:
                log.error(traceback.format_exc())

    def slot_vt_scan_folder_finished(self, videofile_list):

        self.vt_nextuid += len(videofile_list)
        for videofile in videofile_list:
            try:
                item = QListWidgetItem(videofile.definition)
                item.setData(QtCore.Qt.UserRole, videofile)
                self.vt_lstWaiting.addItem(item)
            except Exception:
                log.error(traceback.format_exc())

        self.statusbar.showMessage("Scan Finished!")
        self.vt_change_enable_status_all(True)

    def slot_vt_got_message(self, messagetext):
        try:
            messagetext = messagetext.replace("\n", "")
            if len(str(messagetext).strip(" ")) > len(FRAME_KEYWORD) and str(messagetext).strip(" ")[:len(FRAME_KEYWORD)] == FRAME_KEYWORD:
                self.statusbar.showMessage(messagetext)

                self.vt_sub_transformed_frame = int(messagetext.split("fps")[0].replace(" ", "").replace("frame=", ""))
                self.vt_progressSub.setValue(self.percent(self.vt_sub_transformed_frame, self.vt_sub_total_frame))
            elif "Error" in messagetext:
                log.error(messagetext)
                self.vt_txtProcessOut.appendPlainText(messagetext)
                self.vt_ffmpeg_has_error = True
            else:
                self.vt_txtProcessOut.appendPlainText(messagetext)
        except Exception:
            log.error(traceback.format_exc())

    def slot_vt_single_video_converting(self, videofile):
        try:
            self.vt_ffmpeg_has_error = False
            for index in range(self.vt_lstWaiting.count()):
                item = self.vt_lstWaiting.item(index)
                if item.data(QtCore.Qt.UserRole).uid == videofile.uid:
                    self.vt_lstWaiting.takeItem(self.vt_lstWaiting.row(item))
                    item = QListWidgetItem(videofile.definition)
                    item.setData(QtCore.Qt.UserRole, videofile)
                    self.vt_lstProcessing.addItem(item)
                    break
            self.vt_progressSub.setValue(0)
            self.vt_sub_transformed_frame = 0
            self.vt_sub_total_frame = videofile.video_framecount

            self.vt_txtProcessOut.appendPlainText("_____________________________________________________________")
            self.vt_txtProcessOut.appendPlainText("Converting  >> " + videofile.definition)
            self.vt_txtProcessOut.appendPlainText("_____________________________________________________________")
        except Exception:
            log.error(traceback.format_exc())

    def slot_vt_single_video_converted(self, videofile):
        try:
            for index in range(self.vt_lstProcessing.count()):
                item = self.vt_lstProcessing.item(index)
                if item.data(QtCore.Qt.UserRole).uid == videofile.uid:
                    self.vt_lstProcessing.takeItem(self.vt_lstProcessing.row(item))
                    item = QListWidgetItem(videofile.definition)
                    item.setData(QtCore.Qt.UserRole, videofile)
                    if self.vt_ffmpeg_has_error:
                        item.setForeground(RED)
                    self.vt_lstDone.addItem(item)
                    break
            self.vt_transformed_frame += videofile.video_framecount
            self.vt_progressTotal.setValue(self.percent(self.vt_transformed_frame, self.vt_total_frame))
            self.vt_sub_transformed_frame = 0
            self.vt_sub_total_frame = 0
        except Exception:
            log.error(traceback.format_exc())

    def slot_vt_queue_finished(self):
        try:
            text = self.vt_txtProcessOut.toPlainText()
            text = text.replace("\n\n", "\n")
            self.vt_txtProcessOut.setPlainText(text)

            self.vt_change_enable_status_all(True)
            self.statusbar.showMessage("Video converting finished!")
            self.vt_reset_progress_bars()

        except Exception:
            log.error(traceback.format_exc())

    def vt_change_enable_status_all(self, status):
        try:
            self.vt_btnConvert.setEnabled(status)
            self.vt_btnConvertStop.setEnabled(status)
            self.vt_btnDelete.setEnabled(status)
            self.vt_btnPlay.setEnabled(status)
            self.vt_btnRemove.setEnabled(status)
            self.vt_btnSelectFolder.setEnabled(status)
            self.vt_btnRequeue.setEnabled(status)
        except Exception:
            log.error(traceback.format_exc())

    def vt_reset_progress_bars(self):
        try:
            self.vt_total_frame = 0
            self.vt_transformed_frame = 0
            self.vt_sub_total_frame = 0
            self.vt_sub_transformed_frame = 0
            self.vt_progressTotal.setValue(0)
            self.vt_progressSub.setValue(0)
        except Exception:
            log.error(traceback.format_exc())

    # MULTITOOL

    def event_mt_btnSelectFolder_clicked(self):
        try:
            # noinspection PyCallByClass,PyTypeChecker,PyArgumentList
            directory_path = QFileDialog.getExistingDirectory(self, "Pick Videos Folder")
            self.mt_txtPath.setText(directory_path)
        except Exception:
            log.error(traceback.format_exc())

    def event_mt_btnOrderByNumber_clicked(self):
        try:
            self.statusbar.showMessage("Renaming Files...")
            workpath = os.path.abspath(self.mt_txtPath.text())
            for root, dirs, files in os.walk(workpath, topdown=False):
                maxcount = 0
                for file_name in files:
                    match = re.search("[0-9]+", file_name)
                    if match:
                        if len(match.group()) > maxcount:
                            maxcount = len(match.group())

                for file_name in files:
                    match = re.search("[0-9]+", file_name)
                    if match:
                        otext = match.group()
                        ntext = otext.rjust(maxcount, "0")
                        nfile_name = file_name.replace(otext, ntext)
                        os.rename(os.path.join(root, file_name), os.path.join(root, nfile_name))

            self.statusbar.showMessage("Renaming Finished...")

        except Exception:
            self.statusbar.showMessage("Renaming Finished With Errors!...")
            log.error(traceback.format_exc())

    # LOGS

    def lg_fill_log_list(self):
        self.lg_lstLog.clear()
        f = open(LOG_FILE_PATH, "r")
        for line in f.readlines():
            # part_list = line.split(LOG_SPLITTER)
            # if len(part_list) > 2:
            #     part_list[2] = "".join(part_list[2:])
            #     # noinspection PyUnusedLocal
            #     part_list = part_list[:3]
            litem = QListWidgetItem(line.replace("\n", ""))
            if LOG_SPLITTER + " INFO " + LOG_SPLITTER in line:
                litem.setForeground(GREEN)
            elif LOG_SPLITTER + " WARNING " + LOG_SPLITTER in line:
                litem.setForeground(YELLOW)
            elif LOG_SPLITTER + " ERROR " + LOG_SPLITTER in line:
                litem.setForeground(RED)
            else:
                litem.setForeground(BLUE)
            self.lg_lstLog.addItem(litem)
        f.close()
