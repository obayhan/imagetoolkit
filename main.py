# -*- coding: utf-8 -*-
import logging

from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from ImageToolkitUi import MainWindow  # This file holds our MainWindow and all design related things
from config import LOG_FILE_PATH, LOGFORMAT

__author__ = 'ozgur'
__creation_date__ = '28.08.2016' '14:04'



def main():
    # LOGGING
    # logformat = "%(asctime)s >> %(name)s >> %(levelname)s >> %(message)s"
    logging.basicConfig(filename=LOG_FILE_PATH, level=logging.INFO, filemode="a", format=LOGFORMAT)
    # logging.basicConfig(level=logging.INFO, filemode="a", format=logformat)

    # APPLICATION
    app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
    form = MainWindow()  # We set the form to be our ExampleApp (design)
    form.show()  # Show the form
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function
